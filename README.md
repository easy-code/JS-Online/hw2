## HomeWork #2

##### Cсылка на видео
[Video](https://drive.google.com/open?id=1C31Js_A5EgXWI3BzSX8j1urZUtVZQ8Wc)

##### Ссылки на презентацию
[First](https://docs.google.com/presentation/d/1cv80sdNk-TuU6yzftGjYYEOWDHnSDZlmtlno9obChUU/pub?start=false&loop=false&delayms=3000&slide=id.p)

[Second](https://docs.google.com/presentation/d/1J3y169yeR_nKTM17m1N16D_VYW7jH-Tk1KWrfJ0VvWA/pub?start=false&loop=false&delayms=3000&slide=id.p)

##### Справочники

[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

##### Стиль кода

https://learn.javascript.ru/coding-style

https://eslint.org/docs/rules/multiline-ternary

##### Ссылки из презентации

[Expressions_and_Operators](https://developer.mozilla.org/ru/docs/Web/JavaScript/Guide/Expressions_and_Operators)

[Operator_Precedence](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)

[Operator_Precedence](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)

[Преобразование типов](http://es5.github.io/x11.html#x11.9.3)

[Оператор запятая](https://habrahabr.ru/post/116827/)
