// Констуркция switch... case

// --------------------------------------------------------------------------------------------------------------
// if (a == 'block') {console.log('block')} else
// if (a == 'none') {console.log('none')} else
// if (a == 'inline') {console.log('inline')} else {console.log('other')}
//
// Записать условие, используя конструкцию switch. В консоли должно отоазиться только одно значение.
// --------------------------------------------------------------------------------------------------------------
let a = 'block'; // for example
switch (a) {
    case 'block':
        console.log('block');
        break;
    case 'none':
        console.log('none');
        break;
    case 'inline':
        console.log('inline');
        break;
    default:
        console.log('other');
}
