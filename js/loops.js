// Циклы

// --------------------------------------------------------------------------------------------------------------
// 1. Дана строка "I am in the easycode".
//    Сделать первые буквы каждого слова в верхнем регистре. Использовать for или while
// --------------------------------------------------------------------------------------------------------------
let string = 'I am in the easycode';
let space = ' ';
let output = '';

for (let i = 0; i < string.length; i++) {
    if (string[i - 1] === space) { // проверяем предыдущий символ является ли он пробелом
        output += string[i].toUpperCase(); // выводим символ в верхнем регистре, если перед ним стоит пробел
    } else {
        output += string[i]; // в любом другом случае выводим символ без изменений
    }
}

console.log(output); // I Am In The Easycode

// --------------------------------------------------------------------------------------------------------------
// 2. Дана строка "tseb eht ma i".
//    Используя циклы, сделать строку-перевертыш
//    (то есть последняя юуква становится первой, предпоследняя - второй итд)
// --------------------------------------------------------------------------------------------------------------
let string2 = 'tseb eht ma i';
let output2 = '';

for (let i = string2.length - 1; i >= 0; i--) {
    output2 += string2.charAt(i);
}

console.log(output2); // i am the best

// --------------------------------------------------------------------------------------------------------------
// 3. Факториал числа - произведение всех натуральных чисел от 1 до n включительно: 3! = 3*2*1, 5! = 5*4*3*2*1.
//    С помощью циклов вычислить факториал числа 10.
// --------------------------------------------------------------------------------------------------------------
let value = 10,
    result = value;

while (--value) {
    result *= value;
}
// better?
while (value > 1) {
    result *= value;
    value--;
}

console.log('10! = ' + result); // 10! = 3628800

// --------------------------------------------------------------------------------------------------------------
// 4. Ипользуя циклы, создать строку "Считаем до 10и: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
// --------------------------------------------------------------------------------------------------------------
let total = '',
    comma = ', ';

for (let i = 1; i <= 10; i++) {
    total += i < 10 ? i + comma : i;
}

console.log('Считаем до 10и: ' + total); // Считаем до 10и: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

// --------------------------------------------------------------------------------------------------------------
// 5. На основе строки "JavaScript is a pretty good language" сделать новую строку,
//    где каждое слово начинается с большой буквы, а пробелы удалены
// --------------------------------------------------------------------------------------------------------------
let string5 = 'JavaScript is a pretty good language';
let space5 = ' ';
let output5 = '';

for (let i = 0; i < string5.length; i++) {
    output5 += string5[i] !== space5 ? string5[i - 1] !== space5 ? string5[i] : string5[i].toUpperCase() : '';
}

console.log(output5); // JavaScriptIsAPrettyGoodLanguage

// --------------------------------------------------------------------------------------------------------------
// 6. Найти все нечетные числа от 1 до 15 включительно и вывести их в консоль.
// --------------------------------------------------------------------------------------------------------------
let startRange = 1,
    endRange = 15,
    oddNumbers = '';

for (let i = startRange; i <= endRange; i++) {
    if (i % 2 !== 0) {
        oddNumbers += i < endRange ? i + ', ' : i;
    }
}

console.log('Нечентые числа от 1 до 15 включительно: ' + oddNumbers);
